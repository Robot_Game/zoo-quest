var searchData=
[
  ['interactiontext',['InteractionText',['../class_item.html#a384b7fa799310d40b5bee1355d9b155f',1,'Item.InteractionText()'],['../class_n_p_c.html#aea5f44682bd83851ba98c6def72b1b1e',1,'NPC.InteractionText()']]],
  ['interactswithproximity',['InteractsWithProximity',['../class_item.html#ad92157576e79e9de4ff0ebe66897b532',1,'Item']]],
  ['isconsumedbynpc',['IsConsumedByNPC',['../class_item.html#ad869cd7565ee39f616b476a77d10004a',1,'Item']]],
  ['islocked',['IsLocked',['../class_item.html#aee9d55d4a9c1c78f4f689c7173f0dce7',1,'Item']]],
  ['ispickable',['IsPickable',['../class_item.html#ae548f628e1a6b5a92efeb2eb979943d5',1,'Item']]],
  ['itemname',['ItemName',['../class_item.html#a3bc552a55f37b3639901482d32d467a8',1,'Item']]]
];
