var searchData=
[
  ['iinteractible',['IInteractible',['../interface_i_interactible.html',1,'']]],
  ['instance',['Instance',['../class_canvas_manager.html#a7248458be7d88d307b16c962ea224171',1,'CanvasManager.Instance()'],['../class_inventory.html#a39be28ea481da9a00252ad1cddbfc65c',1,'Inventory.Instance()']]],
  ['interact',['Interact',['../interface_i_interactible.html#aac4b62c4bec829cf757796bd5e3cfead',1,'IInteractible.Interact()'],['../class_item.html#a419a693722f376c3155312399ea8bd86',1,'Item.Interact()'],['../class_n_p_c.html#a44ddbfe02efdae5b7c9bdd259d8f5e4c',1,'NPC.Interact()']]],
  ['interactiontext',['InteractionText',['../interface_i_interactible.html#ac1774dcf9f97e675580aeb1ccc77aade',1,'IInteractible.InteractionText()'],['../class_item.html#a384b7fa799310d40b5bee1355d9b155f',1,'Item.InteractionText()'],['../class_n_p_c.html#aea5f44682bd83851ba98c6def72b1b1e',1,'NPC.InteractionText()']]],
  ['interactswithproximity',['InteractsWithProximity',['../class_item.html#ad92157576e79e9de4ff0ebe66897b532',1,'Item']]],
  ['inventory',['Inventory',['../class_inventory.html',1,'']]],
  ['isconsumedbynpc',['IsConsumedByNPC',['../class_item.html#ad869cd7565ee39f616b476a77d10004a',1,'Item']]],
  ['isgamepaused',['IsGamePaused',['../class_game_manager.html#ad1126f47d6e3e16de48ef3440d9e609e',1,'GameManager']]],
  ['islocked',['IsLocked',['../class_item.html#aee9d55d4a9c1c78f4f689c7173f0dce7',1,'Item']]],
  ['ispickable',['IsPickable',['../class_item.html#ae548f628e1a6b5a92efeb2eb979943d5',1,'Item']]],
  ['item',['Item',['../class_item.html',1,'']]],
  ['itemname',['ItemName',['../class_item.html#a3bc552a55f37b3639901482d32d467a8',1,'Item']]]
];
