﻿# Projeto 3 de LP2 2018 - Zoo Quest

Diogo Martins - a21701345

## My Solution

### Division of Tasks

The entire programming component of the project was done by me alone.

### Architecture

The first think I began to brainstorm was how I would implement NPC's and items in my game. Since those two were the only things the player would be able to interact with and they had some things in common, I used an interface `IInteractible` from which the class `NPC` and `Item` both implement. The `Player` class manages most of the player's behaviours, such has detecting `IInteractibles` with a `Raycast` and calling their respective `Interact()` methods. This was my way of trying to depend as much as possible on abstractions rather than concretions. All the NPC's have an `DialogueAnimation` script and a `QuestManager` script. The `DialogueAnimation` class manages the animations of the NPC's faces (eyes and mouth to be exact), which are sprites. The `QuestManager` class is used to manage when each NPC's quest will be unlocked (because some quests are only unlocked after .`Item`s can be pickable or non-pickable (interaction only). 

I used a Singleton pattern on the `Inventory` and `CanvasManager` classes. The `Inventory` manages a `List` of `Item`s that the player picks and the `CanvasManager` class manages all of the UI elements and animations.

We have a simple `MainMenu` script to manage the main menu interactions (Play, Credits and Quit).

Some of our puzzles required specific scripts such as `BucketsPuzzle` and `TankPanelPuzzle`.

I didn't implement any specific Design Pattern other than the Singleton, but I did follow the SOLID principles as strictly as I could see fit.

### UML Diagram

![Diagram(UML)](https://gitlab.com/Robot_Game/zoo-quest/uploads/b4d59a010b4aa76d758e9eca4c26a40f/Zoo_Quest_UML.png)

## Conclusions

I always love to work on Unity so this was another project that I felt pretty satisfied working on. Although, this time there was a huge difference in my programming mindset thanks to what I've learned the past few months about good programming practices, design patterns and the main principles we should take into account. This was definitely a good challenge that gave me quite more experience and I now hope to continue to improve.

## References

* <a name="ref1">\[1\]</a> Whitaker, R. B. (2016). The C# Player's Guide (3rd Edition). Starbound Software