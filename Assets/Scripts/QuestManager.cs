﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A class that manages the NPC's quests
/// </summary>
public class QuestManager : MonoBehaviour
{
    /// <summary>
    /// A list that contains the locked quests
    /// </summary>
    public List<int> LockedQuests => lockedQuests;
    /// <summary>
    /// An array that contains NPC's whose quests we have to solve before
    /// unlocking this component's NPC quests
    /// </summary>
    public NPC[] RequiredNPC => requiredNPC;
    /// <summary>
    /// The required quest from each NPC
    /// </summary>
    public int[] RequiredQuest => requiredQuest;
    /// <summary>
    /// A list that contains the quests that are locked until the requirements
    /// are completed.
    /// </summary>
    [SerializeField] private List<int> lockedQuests;
    /// <summary>
    /// The NPC's whose quests we have to solve to unlock contents in the
    /// previous variable.
    /// </summary>
    [SerializeField] private NPC[] requiredNPC;
    /// <summary>
    /// The number of the quest from the NPC that we need to solve. Indexes of
    /// this array correspond to the indexes of requiredNPC.
    /// </summary>
    [SerializeField] private int[] requiredQuest;
    /// <summary>
    /// Each index will hold an integer, which will represent the number of
    /// quests that each lockedQuest requires to be unlocked. If, for example,
    /// the first int in lockedQuests requires 2 quests to be solved, then the
    /// index 0 of this array will be a 2.
    /// </summary>
    [SerializeField] private int[] numberOfQuestsRequired; 
    /// <summary>
    /// A counter used to manage the requiredNPC and requiredQuest indexes,
    /// depending on the numberOfQuestRequired values.
    /// </summary>
    private int counter = 0;

    /// <summary>
    /// Method that verifies if the quest is locked
    /// </summary>
    /// <param name="quest">The quest to verify.</param>
    /// <returns>Returns true if the quest is locked</returns>
    public bool QuestIsLocked(int quest)
    {
        // If requirements haven't been solved yet, the quest is locked, so we
        // return true
        if (!RequirementsAreSolved(quest))
        {
            return true;
        }
        // If requirements have been solved we return false, unlocking 
        // the quest
        return false;
    }
    /// <summary>
    /// Method that verifies if the required quests are solved
    /// </summary>
    /// <param name="quest">The quest to verify.</param>
    /// <returns>Returns true if the quests are all solved.</returns>
    private bool RequirementsAreSolved(int quest)
    {
        // If the quest inserted as a parameter belongs in the 
        // lockedQuests List
        if (lockedQuests.Contains(quest))
        {

            int numberOfRequirements =
                numberOfQuestsRequired[lockedQuests.IndexOf(quest)];

            int temporaryCounter = 0;

            // Set a new variable to be equal to the number of requirements
            // that this specific quest has. Those requirements are set in
            // the requiredNPC, requiredQuest and numberOfQuestsRequired
            // variables
            numberOfRequirements =
                numberOfQuestsRequired[lockedQuests.IndexOf(quest)];

            temporaryCounter = counter;

            // Check if the required quests of the specified NPC's are complete
            for (int i = temporaryCounter; i < numberOfRequirements; i++)
            {
                // Make sure that the number of requirements isn't greater 
                // than the number of requiredNPCs so we don't get any
                // crashes
                if (numberOfRequirements <= requiredNPC.Length)
                {
                    // If required NPC has unsolved quests
                    if (!requiredNPC[i].AllQuestsCompleted)
                    {
                        // If at least one of the required quests isn't complete
                        // return false
                        if (requiredNPC[i].CurrentQuest < requiredQuest[i])
                        {
                            return false;
                        }

                    }

                    if (counter < numberOfRequirements - 1)
                        counter++;
                }
                // Warn the developer that he commited a mistake when
                // entering the variables' values
                else Debug.Log("WARNING! The numberOfRequirements is " +
                    "greater than the length of requiredNPC");
            }
        }

        // If we get here it means that either the quest given in the parameter
        // does not belong inside the lockedQuests List or the required quests
        //are all solved
        return true;
    }
}
