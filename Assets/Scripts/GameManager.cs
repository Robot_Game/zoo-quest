﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// The game manager class
/// </summary>
public class GameManager : MonoBehaviour
{
    /// <summary>
    /// The game's main counter
    /// </summary>
    public int MainTimer { get; private set; } = 0;

    public Dictionary<string, int> MonkeyCatchTime { get; private set; } 
        = new Dictionary<string, int>();
    /// <summary>
    /// Boolean property that is set to true if the game is paused
    /// </summary>
    public bool IsGamePaused { get; private set; }
    /// <summary>
    /// Boolean set to true in the inspector while in edit mode for better
    /// game development
    /// </summary>
    [SerializeField] private bool editMode;
    /// <summary>
    /// The max number of monkeys to find, set in the inspector
    /// </summary>
    [SerializeField] private int maxMonkeyCount = 5;
    /// <summary>
    /// The name of the scene to load when the player quits the game
    /// </summary>
    [SerializeField] private string endingSceneName;
    /// <summary>
    /// The name of the scene to load when the game ends
    /// </summary>
    [SerializeField] private string creditsSceneName;
    /// <summary>
    /// The GameObject that is parent of the monkeys' GameObjects
    /// </summary>
    [SerializeField] private GameObject monkeys;
    /// <summary>
    /// The door of the cage where the monkeys will be locked
    /// </summary>
    [SerializeField] private GameObject door;
    /// <summary>
    /// The canvas manager
    /// </summary>
    private CanvasManager canvasManager;
    /// <summary>
    /// An int that holds the number of monkeys caught
    /// </summary>
    private int monkeyCount = 0;
    /// <summary>
    /// A counter that will be rounded
    /// </summary>
    private float currentTime = 0;

    // Unity's standard Start method
    void Start()
    {
        // Get the CanvasManager instance
        canvasManager = CanvasManager.Instance;
        // If we're out of edit mode, make sure the player initial position
        // is inside the monkey's cage
        if (!editMode) SetPlayerInitialPosition();

    }

    // Unity's standard Update method
    void Update()
    {
        IncreaseTimer();
        // Pause or unpause the game if the player presses Esc
        PauseOnInput();
        // Verify if the conditions to end the game have been met and end it if
        // they have
        EndGameOnConditions();
    }
    /// <summary>
    /// A method used to increment the game's main counter
    /// </summary>
    private void IncreaseTimer()
    {
        currentTime += Time.deltaTime;
        MainTimer = Mathf.RoundToInt(currentTime);
        //int hour = Mathf.FloorToInt(currentTime / 3600 % 24);
        //int min = Mathf.FloorToInt(currentTime / 60 % 60);
        //int sec = Mathf.FloorToInt(currentTime % 60);
        //MainTimer = hour.ToString("00") + ":" + min.ToString("00") + ":" + sec.ToString("00");
    }
    /// <summary>
    /// Verify if the conditions to end the game have been met and end it if 
    /// they have
    /// </summary>
    private void EndGameOnConditions()
    {
        // If the number of monkeys caught is equal to the max number of 
        // monkeys
        if (monkeyCount == maxMonkeyCount)
        {
            // Fade out the screen
            ScreenFadeOut();
            SendDataToServer();
            // Start a coroutine to call the credits scene
            StartCoroutine(CreditsScene(4));
        }
        // If the game is paused and player presses Enter
        if (IsGamePaused && Input.GetKey(KeyCode.Return))
        {
            // We need to resume the game before continuing, otherwise the game
            // won't move on (because the TimeScale is set to 0)
            Resume();
            // Fade out the screen
            ScreenFadeOut();
            SendDataToServer();
            // Start a coroutine to call the credits scene
            StartCoroutine(EndScreen(4));
        }
    }
    /// <summary>
    /// Set the player's initial position to be the same as the GameObject 
    /// called "StartingPoint"
    /// </summary>
    private void SetPlayerInitialPosition()
    {
        // Get the player's Transform Component
        Transform player =
            GameObject.FindGameObjectWithTag("Player").transform;
        // Change the player's position to the position of the object called
        // "StartingPoint"
        if (player != null)
            player.transform.position =
                GameObject.Find("StartingPoint").transform.position;
    }
    /// <summary>
    /// Method that is called when the player catches a monkey
    /// </summary>
    public void CaughtMonkey(Item item)
    {
        // Save the monkey's name and the current time to a dictionary
        MonkeyCatchTime.Add(item.name, MainTimer);

        // If it's the first monkey we've caught, close the cage's gate
        if (monkeyCount == 0)
        {
            // Set the trigger that actives the close animation of the door
            door.GetComponent<Animator>().SetTrigger("Close");
        }
        // Activate the monkey in the cage
        monkeys.transform.GetChild(monkeyCount).gameObject.SetActive(true);
        // Set the trigger that activates the monkey's animation
        monkeys.transform.GetChild(monkeyCount)
            .GetComponent<Animator>().SetTrigger("Solved");
        // Increment the caught monkeys' counter
        monkeyCount++;
        // Show through canvas elements how many monkeys have been caught
        canvasManager.ShowMonkeyCount(monkeyCount);
    }
    /// <summary>
    /// A method that makes the screen fade out in black
    /// </summary>
    private void ScreenFadeOut()
    {
        Animator animator = GameObject.Find("BlackScreen")
            .GetComponent<Animator>();

        animator.SetTrigger("FadeOut");
    }
    /// <summary>
    /// A method that loads the ending scene after a few seconds
    /// </summary>
    /// <param name="seconds">The amount of time to wait before loading the 
    /// scene.</param>
    /// <returns></returns>
    IEnumerator EndScreen(float seconds)
    {
        // Wait the given amount of seconds before continuing
        yield return new WaitForSeconds(seconds);
        // Load the ending scene
        SceneManager.LoadScene(endingSceneName);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="seconds">The amount of time to wait before loading the 
    /// scene.</param>
    /// <returns></returns>
    IEnumerator CreditsScene(float seconds)
    {
        // Wait the given amount of seconds before continuing
        yield return new WaitForSeconds(seconds);
        // Load the credits scene
        SceneManager.LoadScene(creditsSceneName);
    }

    private void SendDataToServer()
    {
        Player player = FindObjectOfType<Player>();
        NPC[] npcs = FindObjectsOfType<NPC>();

        for(int i = 0; i < player.InteractionNPC.Count; i++)
        {
            /* SAVE player.InteractionNPC[i] */
            /* SAVE player.InteractionTime[i] */
            // talvez guardar também a interação com items e depois fazer sort 
            // por 'time' no proprio sql
        }

        foreach (NPC npc in npcs)
        {
            foreach(int time in npc.SolvingTime)
            {
                /* SAVE time */
            }
            for (int i = 0; i < npc.SolvingTime.Length; i++)
            {
                /* SAVE npc.SolvingTime[i]; */
            }
        }

        Debug.Log("Playing Time: " + MainTimer);
        Debug.Log("Total interactions: " + FindObjectOfType<Player>().NumberOfInteractions);
        Debug.Log(FindObjectOfType<Player>().NumberOfInteractions);
        Debug.Log(FindObjectOfType<NPC>().SolvingTime);
    }

    /// <summary>
    /// Checks if the player presses Escape and pauses or unpauses the game
    /// </summary>
    private void PauseOnInput()
    {
        // If player presses Escape
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // If the game is paused
            if (IsGamePaused)
            {
                // Resume the game
                Resume();
            }
            // If the game is unpaused
            else
            {
                // Pause the game
                Pause();
            }
        }
    }

    /// <summary>
    /// Method that resumes the game
    /// </summary>
    private void Resume()
    {
        // Hide the pause menu
        canvasManager.ShowPauseMenu(false);
        // Set the timeScale property of Time to 1
        Time.timeScale = 1;
        IsGamePaused = false;
    }

    private void Pause()
    {
        // Show the pause menu
        canvasManager.ShowPauseMenu(true);
        // Set the timeScale property of Time to 0
        Time.timeScale = 0;
        IsGamePaused = true;
    }
}
