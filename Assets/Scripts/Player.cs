﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The class that manages the player
/// </summary>
public class Player : MonoBehaviour
{
    /// <summary>
    /// A variable to count the total number of interactions
    /// </summary>
    public int NumberOfInteractions { get; private set; } = 0;

    public List<string> InteractionNPC { get; private set; } = new List<string>();
    public List<int> InteractionTime { get; private set; } = new List<int>();
    /// <summary>
    /// The max interaction distance set in the inspector
    /// </summary>
    [SerializeField] private float maxInteractionDistance;
    /// <summary>
    /// The size of the inventory
    /// </summary>
    [SerializeField] private int inventorySize;
    /// <summary>
    /// The Player's camera
    /// </summary>
    private Camera cam;
    /// <summary>
    /// The CanvasManager component
    /// </summary>
    private CanvasManager canvasManager;
    /// <summary>
    /// The GameManager component
    /// </summary>
    private GameManager gameManager;
    /// <summary>
    /// A RaycastHit used to detect IInteractibles
    /// </summary>
    private RaycastHit raycastHit;
    /// <summary>
    /// The player's inventory
    /// </summary>
    private Inventory inventory;
    /// <summary>
    /// The IInteractible the player is looking at
    /// </summary>
    private IInteractible currentInteractible;

    /// <summary>
    /// Unity's standard Start method
    /// </summary>
    private void Start()
    {
        // Get the CanvasManager instance
        canvasManager = CanvasManager.Instance;

        // Get the CanvasManager instance
        gameManager = FindObjectOfType<GameManager>();

        // Get the Camera
        cam = GetComponentInChildren<Camera>();
        // Set currentInteractible to null
        currentInteractible = null;

        // Get the Inventory instance
        inventory = Inventory.Instance;
    }

    /// <summary>
    /// Unity's standard Update method
    /// </summary>
    void Update()
    {
        // Check if there are interactibles
        CheckForInteractible();
        // Check if player interacts
        CheckForInteraction();
        // Check player's input
        CheckForInput();
    }
    /// <summary>
    /// Method that checks if there is an interactible in reach
    /// </summary>
    private void CheckForInteractible()
    {
        // If the raycast detects an IInteractible
        if (Physics.Raycast(cam.transform.position,
            cam.transform.forward, out raycastHit,
            maxInteractionDistance))
        {
            // Set the detected IInteractible to a new variable
            IInteractible newInteractible = raycastHit.collider
                .GetComponent<IInteractible>();
            // If there is a newInteractible
            if (newInteractible != null)
                // Set the current interactible to the new one
                SetInteractible(newInteractible);
            else
                // Clear the interactible
                ClearInteractible();
        }
        else
            // Clear the interactible
            ClearInteractible();
    }

    /// <summary>
    /// Check if there is an interaction
    /// </summary>
    private void CheckForInteraction()
    {
        // If player presses E and there is an interactible in reach
        if (Input.GetKeyDown(KeyCode.E) && currentInteractible != null)
        {
            // If the IInteractible is an Item
            if (currentInteractible is Item)
            {
                // Get the Item component
                Item item =
                    currentInteractible.GameObject.GetComponent<Item>();
                // If the item is unlocked and the player has the required 
                // items to interact with this item 
                if (HasRequirements(item) && !item.IsLocked)
                {
                    // Interact with the item
                    Interact(item);
                    // If the item is pickable, pick it
                    if (item.IsPickable)
                    {
                        AddToInventory(item);
                        Debug.Log("[" + gameManager.MainTimer + "] " +
                        "Player picked " + item.ItemName);
                    }
                    else Debug.Log("[" + gameManager.MainTimer + "] " +
                        "Player interacted with " + item.ItemName);
                }
            }
            // If the IInteractible is an NPC
            if (currentInteractible is NPC)
            {
                // Get the NPC's component
                NPC npc = currentInteractible.GameObject.GetComponent<NPC>();

                // Save the NPC name and the current time of the game to a dictionary
                SaveNPCInteractionTime(npc);

                // Call the NPC's Interact method
                npc.Interact();
            }
            // Increment the total number of interactions
            NumberOfInteractions++;
            Debug.Log("Number of interactions: " + NumberOfInteractions);
        }
    }

    /// <summary>
    /// Check the player's input
    /// </summary>
    private void CheckForInput()
    {
        // If player presses Tab
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            // Show inventory
            inventory.ShowInventory();

            Debug.Log("[" + gameManager.MainTimer + "] " +
                        "Player opened inventory");
        }
        // If player drops Tab
        if (Input.GetKeyUp(KeyCode.Tab))
        {
            // Hide inventory
            inventory.HideInventory();
        }
    }
    /// <summary>
    /// Method that sets a new interactible
    /// </summary>
    /// <param name="newInteractible">The new interactible holding variable.
    /// </param>
    private void SetInteractible(IInteractible newInteractible)
    {
        // Set the currentInteractible to the newInteractible
        currentInteractible = newInteractible;

        // If IInteractible is an Item
        if (currentInteractible is Item)
        {
            // Get the Item component
            Item item = currentInteractible.GameObject.GetComponent<Item>();

            // If the item is enabled
            if (item.enabled)
            {
                // Show interaction text if player has requirements and the item
                // is unlocked
                if (HasRequirements(item) && !item.IsLocked
                    && item.InteractionText.Length > 0)
                {
                    canvasManager.ShowInteractionPanel(item.InteractionText);
                }
                // Else show the requirement text
                else if (!HasRequirements(item) || (item.IsLocked
                    && !item.WasInteracted && item.RequirementText.Length > 0))
                {
                    canvasManager.ShowInteractionPanel(item.RequirementText);
                }
                // Hide interaction panel
                else canvasManager.HideInteractionPanel();
            }
        }
        // If the IInteractible is an NPC
        if (currentInteractible is NPC)
        {
            NPC npc = currentInteractible.GameObject.GetComponent<NPC>();

            // If player is no longer talking to the NPC
            if (npc.ConversationEnded)
            {
                //If player hasn't completed all the quests
                if (!npc.AllQuestsCompleted)
                {
                    if (npc.NumberOfQuests == 0)
                        canvasManager.ShowInteractionPanel(
                            npc.DefaultInteractionText);
                    // If player hasn't completed the quest yet show default 
                    // interaction text
                    else if (!HasInteracted(npc.QuestObjects[npc.CurrentQuest]))
                        canvasManager.ShowInteractionPanel(
                            npc.DefaultInteractionText);
                    // Else show the interaction text related to the current 
                    // quest
                    else canvasManager
                            .ShowInteractionPanel(npc.InteractionText);
                }
                // If player completed all the quests, show default interaction
                else canvasManager.ShowInteractionPanel(
                    npc.DefaultInteractionText);
            }
            else
            {
                // Hide interaction panel
                canvasManager.HideInteractionPanel();
            }
        }
    }
    /// <summary>
    /// Method that clears the interactible variable
    /// </summary>
    private void ClearInteractible()
    {
        currentInteractible = null;
        canvasManager.HideInteractionPanel();
    }
    /// <summary>
    /// Method that plays the interaction
    /// </summary>
    private void Interact(IInteractible interactible)
    {
        // Get the Item component
        Item item = currentInteractible.GameObject.GetComponent<Item>();

        if (item.ConsumesRequirements && item.RequiredItems.Length > 0)
        {
            // Go through all the required items necessary to interact 
            // with this item
            for (int i = 0; i < item.RequiredItems.Length; i++)
            {
                // Remove it from the inventory (but first make sure 
                // the item is in it
                if (HasInInventory(item.RequiredItems[i]))
                    RemoveFromInventory(item.RequiredItems[i]);
            }
        }
        // Interact
        interactible.Interact();
    }

    /// <summary>
    /// Method that checks if the player has the requirements to interact
    /// </summary>
    /// <param name="item">The item we want to check.</param>
    /// <returns></returns>
    public bool HasRequirements(Item item)
    {
        // If the item has more than one required item
        if (!item.OnlyOneRequiredItemNeeded)
        {
            // Go through all the required items necessary to interact with 
            // this item if there are any
            if (item.RequiredItems.Length > 0)
            {
                for (int i = 0; i < item.RequiredItems.Length; i++)
                {
                    // If one of them wasn't interacted yet return false
                    if (!item.RequiredItems[i].WasInteracted) return false;
                }
            }
            // If there weren't any requirements or the player didn't get them 
            // all yet
            return true;
        }
        // If item only needs one requirement
        else
        {
            // Go through all the required items necessary to interact with 
            // this item if there are any
            if (item.RequiredItems.Length > 0)
            {
                for (int i = 0; i < item.RequiredItems.Length; i++)
                {
                    // If one of them was interacted return true
                    if (item.RequiredItems[i].WasInteracted) return true;
                }
            }
            // If there weren't any requirements
            return false;
        }
    }
    /// <summary>
    /// Method that adds the pickable item to the player's inventory
    /// </summary>
    /// <param name="pickable">The pickable item.</param>
    private void AddToInventory(Item pickable)
    {
        pickable.Interact();
        inventory.Add(pickable);
    }
    /// <summary>
    /// Method that returns true if an item has been interacted.
    /// </summary>
    /// <param name="item">The item to verify.</param>
    /// <returns>Returns true if the item has been interacted.</returns>
    public bool HasInteracted(Item item)
    {
        if (item.WasInteracted) return true;

        return false;
    }
    /// <summary>
    /// Method that verifies if an item is in the player's inventory
    /// </summary>
    /// <param name="pickable">The item to check.</param>
    /// <returns>Returns true if the item is in the inventory.</returns>
    public bool HasInInventory(Item pickable)
    {
        return inventory.Contains(pickable);
    }
    /// <summary>
    /// Removes an item from the player's inventory
    /// </summary>
    /// <param name="pickable">The item to remove.</param>
    public void RemoveFromInventory(Item pickable)
    {
        inventory.Remove(pickable);
    }
    /// <summary>
    /// Method that saves the interacted NPC's name and the time at which the
    /// player interacted with it
    /// </summary>
    /// <param name="npc"></param>
    private void SaveNPCInteractionTime(NPC npc)
    {
        InteractionNPC.Add(npc.name);
        InteractionTime.Add(gameManager.MainTimer);
    }
}
