﻿using UnityEngine;
/// <summary>
/// Method that manages the lights of the panel from the Dolphin and Sea Lion
/// puzzle
/// </summary>
public class PanelLights : MonoBehaviour {

    /// <summary>
    /// Whether this light is active or not
    /// </summary>
    public bool Active => active;
    /// <summary>
    /// Whether this light is active or not
    /// </summary>
    private bool active;
    /// <summary>
    /// The renderer component of this light
    /// </summary>
    private Renderer rend;
    /// <summary>
    /// A green material
    /// </summary>
    [SerializeField] private Material greenMat;
    /// <summary>
    /// A red material
    /// </summary>
    [SerializeField] private Material redMat;

    /// <summary>
    /// Unity's standard Start method
    /// </summary>
    void Start () {
        //Fetch the Renderer from the GameObject
        rend = GetComponent<Renderer>();
    }
    /// <summary>
    /// Change the state of the light
    /// </summary>
    /// <param name="activate">Whether to activate or deactivate the light.
    /// </param>
    public void ChangeState(bool activate)
    {
        // If it should be activated
        if(activate)
        {
            // Activate it and set it to green
            active = true;
            SetToGreen();
        }
        else
        {
            // Deactivate it and set it to red
            active = false;
            SetToRed();
        }
    }
    /// <summary>
    /// Change the material of the light to red
    /// </summary>
    private void SetToRed()
    {
        //Change the material to the red material
        rend.material = redMat;
    }
    /// <summary>
    /// Change the material of the light to green
    /// </summary>
    private void SetToGreen()
    {
        //Change the material to the green material
        rend.material = greenMat;
    }


}
