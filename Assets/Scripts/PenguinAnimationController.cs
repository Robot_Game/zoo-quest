﻿using UnityEngine;

/// <summary>
/// The animator controller for the Penguin NPC
/// </summary>
public class PenguinAnimationController : MonoBehaviour {

    /// <summary>
    /// The penguin NPC
    /// </summary>
    [SerializeField] private NPC penguin;
    /// <summary>
    /// The penguin's Animator
    /// </summary>
    [SerializeField] private Animator penguinAnimator;
    /// <summary>
    /// The penguin's new position
    /// </summary>
    [SerializeField] private Transform penguinNewPos;

    /// <summary>
    /// A flag used to play the anims only when needed
    /// </summary>
    private bool animFlag = false;

    /// <summary>
    /// Unity's standard Start method
    /// </summary>
    void Start ()
    {
        // Set the penguin's animator to false
        penguinAnimator.enabled = false;
	}

    /// <summary>
    /// Unity's standard Update method
    /// </summary>
    void Update () {

        // If all of the penguin's quests have been completed and
        // the flag is false
        if (penguin.AllQuestsCompleted && !animFlag)
        {
            // Enable penguin's animator
            penguinAnimator.enabled = true;
            // Play penguin's animation
            penguinAnimator.SetTrigger("Solved");
            // Set the animation flag to true
            animFlag = true;
        }
        // If the penguin is positioned in the new position
        if (penguin.transform.position == penguinNewPos.position)
        {
            // Disable penguin's animator
            penguinAnimator.enabled = false;
        }
    }
}
