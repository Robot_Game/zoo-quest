﻿using UnityEngine;

/// <summary>
/// IInteractible class that all objects that the player interacts (except the 
/// NPCs) implement.
/// </summary>
public class Item : MonoBehaviour, IInteractible
{
    /// <summary>
    /// A property that points to the GameObject of this istance
    /// </summary>
    GameObject IInteractible.GameObject => gameObject;
    /// <summary>
    /// A property that describes the name of this item
    /// </summary>
    public string ItemName => itemName;
    /// <summary>
    /// A property that defines whether this item is locked or unlocked
    /// </summary>
    public bool IsLocked => isLocked;
    /// <summary>
    /// Whether this item unlocks with player's proximity or not
    /// </summary>
    public bool UnlocksWithProximity => unlocksWithProximity;
    /// <summary>
    /// Whether this item interacts with player's proximity or not
    /// </summary>
    public bool InteractsWithProximity => interactsWithProximity;
    /// <summary>
    /// Whether this item has infinite interactions or not
    /// </summary>
    public bool HasInfiniteInteractions => hasInfiniteInteractions;
    /// <summary>
    /// Whether this item is pickable or not
    /// </summary>
    public bool IsPickable => isPickable;
    /// <summary>
    /// Whether this item is consumed by the NPC or not
    /// </summary>
    public bool IsConsumedByNPC => isConsumedByNPC;
    /// <summary>
    /// The text that shows when this item is still locked
    /// </summary>
    public string RequirementText => requirementText;
    /// <summary>
    /// The items the player needs on his inventory to interact with this item
    /// </summary>
    public Item[] RequiredItems => requiredItems;
    /// <summary>
    /// Whether the player only needs one item from the required list, or all
    /// of them
    /// </summary>
    public bool OnlyOneRequiredItemNeeded => onlyOneRequiredItemNeeded;
    /// <summary>
    /// Whether the player consumes the items required
    /// </summary>
    public bool ConsumesRequirements => consumesRequirements;
    /// <summary>
    /// The text that shows when the player can interact with the item
    /// </summary>
    public string InteractionText => interactionText;
    /// <summary>
    /// Whether the item was interacted or not
    /// </summary>
    public bool WasInteracted => wasInteracted;

    /// <summary>
    /// The name of the item
    /// </summary>
    [SerializeField] private string itemName;
    /// <summary>
    /// Whether this item is locked or unlocked
    /// </summary>
    [SerializeField] private bool isLocked;
    /// <summary>
    /// Whether this item has infinite interactions or not
    /// </summary>
    [SerializeField] private bool hasInfiniteInteractions;
    /// <summary>
    /// Whether this item is pickable or not
    /// </summary>
    [SerializeField] private bool isPickable;
    /// <summary>
    /// Whether this item is consumed by the NPC or not
    /// </summary>
    [SerializeField] private bool isConsumedByNPC;

    /// <summary>
    /// The text that shows when this item is still locked
    /// </summary>
    [SerializeField] private string requirementText;
    /// <summary>
    /// The items the player needs on his inventory to interact with this item
    /// </summary>
    [SerializeField] private Item[] requiredItems;
    /// <summary>
    /// Whether the player only needs one item from the required list, or all
    /// of them
    /// </summary>
    [SerializeField] private bool onlyOneRequiredItemNeeded;
    /// <summary>
    /// Whether the player consumes the items required
    /// </summary>
    [SerializeField] private bool consumesRequirements;

    /// <summary>
    /// The text that shows when the player can interact with the item
    /// </summary>
    [SerializeField] private string interactionText;
    /// <summary>
    /// Whether this item unlocks with player's proximity or not
    /// </summary>
    [SerializeField] private bool unlocksWithProximity;
    /// <summary>
    /// Whether this item interacts with player's proximity or not
    /// </summary>
    [SerializeField] private bool interactsWithProximity;
    /// <summary>
    /// Whether this item is disabled after the interaction or not
    /// </summary>
    [SerializeField] private bool disablesAfterInteraction;
    /// <summary>
    /// The items that are unlocked when we interact with this item
    /// </summary>
    [SerializeField] private Item[] indirectInteractionUnlocks;
    /// <summary>
    /// The items that are unlocked when we unlock this item
    /// </summary>
    [SerializeField] private Item[] indirectUnlocks;
    /// <summary>
    /// Whether this item was interacted or not
    /// </summary>
    private bool wasInteracted = false;
    /// <summary>
    /// The animator component of this item
    /// </summary>
    private Animator animator;

    /// <summary>
    /// Unity's standart Start method
    /// </summary>
    private void Start()
    {
        // In case we forgot to assign a name in the inspector we set it to the
        // GameObject's name
        if (itemName == null) itemName = gameObject.name;

        // Get the animator component of this item
        animator = GetComponent<Animator>();
    }
    /// <summary>
    /// The method that creates the interaction
    /// </summary>
    public void Interact()
    {
        // A flag to manage the states of the interaction
        bool flag = false;
        // If this item only interacts once
        if (!hasInfiniteInteractions)
        {
            // Play the interaction animation
            PlayInteractAnimation();
            // Set wasInteracted to true
            wasInteracted = true;
            // Unlock the items that are unlocked when we interact with this
            // item
            UnlockIndirectsOnInteraction();
            // If the item disables after the interaction
            if (disablesAfterInteraction)
            {
                // Lock the item
                Unlock(false);
            }
        }
        // If the item has multiple interactions and it has been interacted 
        // before
        else if(wasInteracted && !flag)
        {
            // Set flag to true
            flag = true;
            // Set wasInteracted to false
            wasInteracted = false;
            // Play the deactivation animation
            PlayDeactiveAnimation();
        }
        // If the item has multiple interactions and it hasn't been interacted 
        // yet
        else if (!wasInteracted && !flag)
        {
            // Set wasInteracted to true
            wasInteracted = true;
            // Play activation animation
            PlayActiveAnimation();
        }
    }
    /// <summary>
    /// Method that unlocks or locks this item
    /// </summary>
    /// <param name="value">Whether to unlock the item or lock it.</param>
    public void Unlock(bool value)
    {
        //If the parameter is true
        if (value == true)
        {
            // Unlock items that should be unlocked when this item is unlocked
            UnlockIndirects();
            // Play this item's unlock animation
            PlayUnlockAnimation();
            // Set is locked to true
            isLocked = false;
        }
        // If the parameter is false
        else
        {
            // Play lock animation
            PlayLockAnimation();
            // Set isLocked to true
            isLocked = true;
        }
    }

    /// <summary>
    /// A Unity's standard method used to check if a collision happens
    /// </summary>
    /// <param name="playerCollider">The collider of the player.</param>
    private void OnTriggerEnter(Collider playerCollider)
    {
        // If this item unlocks with proximity and it's colliding with
        // the player
        if (unlocksWithProximity && playerCollider.tag == "Player")
        {
            // Play unlock animation
            PlayUnlockAnimation();
        }
        // If this item interacts with proximity and it's colliding with
        // the player
        if (interactsWithProximity && playerCollider.tag == "Player")
        {
            // Play interaction animation
            PlayInteractAnimation();

            // If disables after interaction, set interactsWithProximity to
            // false
            if (disablesAfterInteraction) interactsWithProximity = false;
        }
    }
    /// <summary>
    /// A Unity's standard method used to check if a collision stops
    /// </summary>
    /// <param name="playerCollider">The collider of the player.</param>
    private void OnTriggerExit(Collider playerCollider)
    {
        // If this item unlocks with proximity and it collides with the player
        if (unlocksWithProximity && playerCollider.tag == "Player")
        {
            // Play lock animation
            PlayLockAnimation();
        }
    }
    /// <summary>
    /// A method that plays the interaction animation
    /// </summary>
    private void PlayInteractAnimation()
    {
        // Set the interaction trigger of the animator
        if (animator != null)
            animator.SetTrigger("Interact");
    }
    /// <summary>
    /// A method that plays the unlock animation
    /// </summary>
    private void PlayUnlockAnimation()
    {        
        // Set the unlock trigger of the animator
        if (animator != null)
            animator.SetTrigger("Unlock");
    }
    /// <summary>
    /// A method that plays the lock animation
    /// </summary>
    private void PlayLockAnimation()
    {
        // Set the lock trigger of the animator
        if (animator != null)
            animator.SetTrigger("Lock");
    }
    /// <summary>
    /// A method that plays the activation animation
    /// </summary>
    private void PlayActiveAnimation()
    {
        // Set the activate trigger of the animator
        if (animator != null)
            animator.SetTrigger("Activate");
    }
    /// <summary>
    /// A method that plays the deactivation animation
    /// </summary>
    private void PlayDeactiveAnimation()
    {
        // Set the deactivate trigger of the animator
        if (animator != null)
            animator.SetTrigger("Deactivate");
    }
    /// <summary>
    /// Method that unlocks the items that should be unlocked when the player
    /// interacts with this item
    /// </summary>
    private void UnlockIndirectsOnInteraction()
    {
        if (indirectInteractionUnlocks != null)
        {
            // Unlock all the items in the indirectInteractionUnlocks array
            for (int i = 0; i < indirectInteractionUnlocks.Length; ++i)
                indirectInteractionUnlocks[i].Unlock(true);
        }
    }
    /// <summary>
    /// Method that unlocks the items that should be unlocked when this item
    /// is unlocked
    /// </summary>
    private void UnlockIndirects()
    {
        if (indirectUnlocks != null)
        {
            // Unlock all the items in the indirectUnlocks array
            for (int i = 0; i < indirectUnlocks.Length; ++i)
                indirectUnlocks[i].Unlock(true);
        }
    }
}
