﻿using UnityEngine;

/// <summary>
/// An interface for all the objects with which the player can interact
/// </summary>
public interface IInteractible
{
    /// <summary>
    /// A property used to get the GameObject component of the IInteractible
    /// </summary>
    GameObject GameObject { get; }
    /// <summary>
    /// The IInteractible's interaction text
    /// </summary>
    string InteractionText { get; }
    /// <summary>
    /// Method that defines the interaction of the IInteractible 
    /// </summary>
    void Interact();
}
