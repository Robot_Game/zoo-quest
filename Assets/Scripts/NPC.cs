﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The class that manages all of the NPC behaviours
/// </summary>
public class NPC : MonoBehaviour, IInteractible
{
    /// <summary>
    /// A property that points to the GameObject of this istance
    /// </summary>
    GameObject IInteractible.GameObject => gameObject;
    /// <summary>
    /// A property that holds the current quest number
    /// </summary>
    public int CurrentQuest => currentQuest;
    /// <summary>
    /// A property that holds the total number of quests
    /// </summary>
    public int NumberOfQuests => numberOfQuests;
    /// <summary>
    /// Whether the conversation with the NPC has ended or not
    /// </summary>
    public bool ConversationEnded => conversationEnded;
    /// <summary>
    /// Whether the current quest has been completed or not
    /// </summary>
    public bool CurrentQuestCompleted => currentQuestCompleted;
    /// <summary>
    /// Whether all the quests have been completed or not
    /// </summary>
    public bool AllQuestsCompleted => allQuestsCompleted;
    /// <summary>
    /// The current interaction text for this npc
    /// </summary>
    public string InteractionText => interactionText[currentQuest + 1];
    /// <summary>
    /// The default interaction text for this npc
    /// </summary>
    public string DefaultInteractionText => interactionText[0];
    /// <summary>
    /// The objects necessary to complete each quest
    /// </summary>
    public Item[] QuestObjects => questObjects;

    public int[] SolvingTime { get; private set; }

    /// <summary>
    /// An array that contains all the text for when there is no riddle 
    /// available
    /// </summary>
    [Header("Texts")]
    [Tooltip("The text that shows when there is no riddle.")]
    [SerializeField, TextArea(1, 4)]
    private string[] noRiddleText;
    /// <summary>
    /// An array that contains all of the quests
    /// </summary>
    [Tooltip("Each index of this array is a different quest.")]
    [SerializeField, TextArea(1, 4)]
    private string[] quests;
    /// <summary>
    /// An array that contains all the dialogues given after completing each 
    /// quest
    /// </summary>
    [Tooltip("The dialogue given after completing each quest.")]
    [SerializeField, TextArea(1, 4)]
    private string[] completionText;
    /// <summary>
    /// An array that contains the interactions texts
    /// </summary>
    [Tooltip("The first index represents the default interaction text, the " +
        "others relate to their respective quest.")]
    [SerializeField, TextArea(1, 4)]
    private string[] interactionText;
    /// <summary>
    /// The items necessary to complete each quest
    /// </summary>
    [Header("Other variables")]
    [Tooltip("The objects of each quest")]
    [SerializeField]
    private Item[] questObjects;
    /// <summary>
    /// An array that contains the items that are animated when the quest is
    /// completed
    /// </summary>
    [Tooltip("The objects that will be animated when the quest is complete")]
    [SerializeField]
    private Item[] questCompletionAnimatedObjects;
    /// <summary>
    /// A counter used to manage which noRiddleText to show
    /// </summary>
    private int counter = 0;
    /// <summary>
    /// The current quest
    /// </summary>
    private int currentQuest;
    /// <summary>
    /// The total number of quests
    /// </summary>
    private int numberOfQuests;
    /// <summary>
    /// Whether there is an active quest or not
    /// </summary>
    private bool questActive = false;
    /// <summary>
    /// Whether the conversation has ended or not
    /// </summary>
    private bool conversationEnded = true;
    /// <summary>
    /// Whether the current quest is completed or not
    /// </summary>
    private bool currentQuestCompleted = false;
    /// <summary>
    /// Whether all the quests have been completed or not
    /// </summary>
    private bool allQuestsCompleted = false;
    /// <summary>
    /// The distance between the player and the NPC
    /// </summary>
    private float distance;
    /// <summary>
    /// The NPC's quest panel
    /// </summary>
    private GameObject questPanel;
    /// <summary>
    /// The Transform component of the player
    /// </summary>
    private Transform player;
    /// <summary>
    /// The QuestManager component attached to this NPC
    /// </summary>
    private QuestManager questManager;
    /// <summary>
    /// The GameManager component
    /// </summary>
    private GameManager gameManager;
    /// <summary>
    /// A flag used to manage the state of the riddle panel
    /// </summary>
    private int riddlePanelFlag;
    /// <summary>
    /// A flag used to manage the state of the exclamation point
    /// </summary>
    private int exclamationFlag;
    /// <summary>
    /// A timer used to animate the NPC's faces and sounds
    /// </summary>
    private float timer;
    /// <summary>
    /// A timer used to count how much time the player takes to solve a quest
    /// </summary>
    private float questTimer;
    /// <summary>
    /// This NPC's AudioSource component
    /// </summary>
    private AudioSource audioSource;
    /// <summary>
    /// This NPC's DialogueAnimation component
    /// </summary>
    private DialogueAnimation faceAnims;
    /// <summary>
    /// Whether or not to stop the animations
    /// </summary>
    private bool stopAnims = true;

    /// <summary>
    /// Unity's standard Start method
    /// </summary>
    void Start()
    {
        // Get the audio source
        audioSource = GetComponent<AudioSource>();

        //Get the quest panel GameObject
        questPanel =
            transform.Find("RiddlePanelHolder").GetChild(0).gameObject;

        // Get the Player's Transform component
        player = GameObject.FindGameObjectWithTag("Player").transform;

        // Get the QuestManager component
        questManager = GetComponent<QuestManager>();

        // Get the CanvasManager instance
        gameManager = FindObjectOfType<GameManager>();

        // Set the number of quests to the length of the questText array
        numberOfQuests = quests.Length;

        // Get the DialogueAnimation component
        faceAnims = GetComponent<DialogueAnimation>();

        // Inicialize currentQuest at 0
        currentQuest = 0;

        // Set the solving time array to have the same number of indexes as quests' array
        SolvingTime = new int[quests.Length];

        // Set exclamationFlag to zero
        exclamationFlag = 0;

        // Warn if number of quests is not the same as number of quest objects
        if (quests.Length != questObjects.Length)
            Debug.Log("Careful, " + gameObject.name + "number of riddles " +
                "and quest objects aren't the same!");

        // Lock all objects when the game begins
        for (int i = 0; i < questObjects.Length; i++)
        {
            questObjects[i]?.Unlock(false);
        }
    }

    // Unity's standard Update method
    void Update()
    {
        // Check the distance between the NPC and the player
        CheckDistanceFromPlayer();
        // Show this NPC's animations
        ShowAnimations();
    }
    /// <summary>
    /// The interaction method
    /// </summary>
    public void Interact()
    {
        // Shows the riddle panel of this NPC
        ShowRiddlePanel();
    }
    /// <summary>
    /// Method that manages and shows all of this NPC's animations
    /// </summary>
    private void ShowAnimations()
    {
        // If the distance is bigger than 40
        if (distance > 40)
        {
            // Hide the exclamation point if the flag is 1 or there are no 
            // quests
            if (exclamationFlag == 1 || quests.Length == 0)
                HideExclamationPoint();
        }
        // If distance is smaller than 40
        else
        {
            // Turn the NPC to the player
            TurnToPlayer();
            //If exclamation flag is 1 or there are no quests
            if (exclamationFlag == 1 || quests.Length == 0)
            {
                // If there isn't an available quest, or all the quests are 
                // completed or player is talking to an NPC
                if (!IsQuestUnlocked() || allQuestsCompleted || !conversationEnded)
                {
                    // Hide the exclamation point
                    HideExclamationPoint();
                }
            }
            // If exlcamationFlag is zero and there are quests
            if (exclamationFlag == 0 && quests.Length > 0)
            {
                // If there is a quest unlocked, there are quests to solve and
                // the player isn't talking to the NPC
                if (IsQuestUnlocked() && !allQuestsCompleted
                    && conversationEnded)
                {
                    // Show the exclamation point
                    ShowExclamationPoint();
                }
            }

            // If animations should be running
            if (!stopAnims)
            {
                // Increment the timer using Time.deltaTime
                timer += Time.deltaTime;

                // Show face animations while the timer is smaller than the 
                // quest text divided by 18
                if (timer < questPanel.GetComponentInChildren<TextMesh>()
                    .text.Length / 18)
                {
                    faceAnims.ShowAnimations(timer);

                }
                // If the timer is bigger than the quest text divided by 18
                else
                {
                    // If all quests have been completed
                    if (allQuestsCompleted)
                    {
                        // Change the default faces and make the mouth visible
                        faceAnims.SetDefaultFaces(0, 2, true);
                        // Stop this NPC's audio
                        audioSource.Stop();
                        // Set stopAnims to true
                        stopAnims = true;
                    }
                    // If there are still quests to complete
                    else
                    {
                        // Change the default faces and make the mouth 
                        // invisible
                        faceAnims.SetDefaultFaces(0, 0, false);
                        // Stop this NPC's audio
                        audioSource.Stop();
                        // Set stopAnims to true
                        stopAnims = true;
                    }
                }
            }
        }
        // If the distance is bigger than 8 or player presses left mouse 
        // button hide riddle pannel
        if (distance > 8 || Input.GetMouseButtonDown(0)) HideRiddlePanel();
    }
    /// <summary>
    /// Method that shows the NPC's riddle panel
    /// </summary>
    public void ShowRiddlePanel()
    {
        // If the flag is zero (we use this flag so that this is only called 
        // once
        if (riddlePanelFlag == 0)
        {

            // Set conversasionEnded to false
            conversationEnded = false;
            // Set the Show trigger of the quest panel's animator
            questPanel.GetComponent<Animator>().SetTrigger("Show");
            // Play the dialogue sound
            audioSource.Play();

            // If player hasn't completed all the quests from this NPC
            if (!allQuestsCompleted)
            {
                // If there is an active quest from this NPC
                if (questActive)
                {
                    // If player has the requirements to complete the quest
                    if (player.GetComponent<Player>()
                        .HasInteracted(questObjects[currentQuest]))
                    {
                        // Show grateful text
                        if (completionText[currentQuest] != null)
                        {
                            questPanel.GetComponentInChildren<TextMesh>().text
                                 = completionText[currentQuest];
                        }

                        //Just in case we forgot to insert the text
                        else
                        {
                            Debug.Log(gameObject.name + " has no grateful text assigned for" +
                                "this quest completion! Go check it out!");

                            questPanel.GetComponentInChildren<TextMesh>().text = "Thanks!";
                        }

                        // If the quest object is consumed by the NPC
                        // remove it from the inventory
                        if (questObjects[currentQuest].IsConsumedByNPC)
                        {
                            player.GetComponent<Player>()
                                .RemoveFromInventory(questObjects[currentQuest]);
                        }

                        //Debug.Log("[" + gameManager.MainTimer + "] " +
                        //"Player has delivered the " +
                        //questObjects[currentQuest].name + " to " + gameObject.name);

                        // Stop the coroutine that increments the 
                        StopAllCoroutines();
                        SaveTime();
                        ResetTime();

                        // Change the current quest number
                        ChangeQuest();


                    }
                    // If the player doesn't have the requirements to complete
                    // the quest
                    else
                    {
                        // Set panel's text to it's current quest
                        questPanel.GetComponentInChildren<TextMesh>().text
                            = quests[currentQuest];
                    }
                }
                // If there is no active quest from this NPC yet
                else
                {
                    //If the quest is unlocked
                    if (IsQuestUnlocked())
                    {
                        // Set panel's text to it's current quest
                        questPanel.GetComponentInChildren<TextMesh>().text
                            = quests[currentQuest];

                        // If the current quest object exists and is locked
                        if (questObjects[currentQuest] != null
                            && questObjects[currentQuest].IsLocked)
                        {
                            // Unlock the object
                            questObjects[currentQuest].Unlock(true);
                        }
                        else
                        {
                            // Message for bug detection
                            Debug.Log(gameObject.name + " has no quest object for this quest!" +
                                "Don't forget to assign it!");
                        }
                        // Set questActive to true
                        questActive = true;
                        // Increment the counter
                        counter++;

                        //Debug.Log("[" + gameManager.MainTimer + "] " +
                        //    "Player was given " + name + "'s quest number " + (currentQuest + 1));

                        StartCoroutine(IncrementTimer());
                    }
                    // If the quest is locked
                    else
                    {
                        // Show the no riddle text
                        questPanel.GetComponentInChildren<TextMesh>().text
                            = noRiddleText?[counter] ?? noRiddleText[0];

                        //Debug.Log("[" + gameManager.MainTimer + "] " +
                        //"Player spoke to " + name);
                    }
                }
            }
            // Increment the riddlePanelFlag
            riddlePanelFlag++;
        }
        // Set stopAnims to false;
        stopAnims = false;
    }
    /// <summary>
    /// Method that hides the riddle panel
    /// </summary>
    private void HideRiddlePanel()
    {
        stopAnims = true;
        timer = 0;
        // If all quests have been completed
        if (allQuestsCompleted)
        {
            // Change the default faces and show mouth
            faceAnims.SetDefaultFaces(0, 2, true);
            // Stop this NPC's dialogue sound
            audioSource.Stop();
        }
        // If there are still quests to complete
        else
        {
            // Change the default faces and hide mouth
            faceAnims.SetDefaultFaces(0, 0, false);
            // Stop this NPC's dialogue sound
            audioSource.Stop();
        }
        // If the riddlePanelFlag is bigger than zero when this method is 
        // called
        if (riddlePanelFlag > 0)
        {
            // Set conversasionEnded to true
            conversationEnded = true;
            // Set the Hide trigger of the quest panel's animator
            questPanel.GetComponent<Animator>().SetTrigger("Hide");
            // Set riddlePanelFlag back to zero
            riddlePanelFlag = 0;
        }
    }
    /// <summary>
    /// Method that tells whether there is an unlocked quest or not
    /// </summary>
    /// <returns>Returns true if there is a quest unlocked.</returns>
    private bool IsQuestUnlocked()
    {
        // If this NPC has quests
        if (quests.Length > 0)
        {
            // Return true if quest is unlocked
            if (!questManager.QuestIsLocked(currentQuest + 1))
            {
                return true;
            }
        }

        return false;
    }
    /// <summary>
    /// Method that changes the current quest
    /// </summary>
    public void ChangeQuest()
    {
        //If NPC has more quests to offer, increment current quest value
        if (currentQuest < numberOfQuests && !allQuestsCompleted)
        {
            currentQuest++;
        }
        // If current quest is equal to the total number of quests
        if (currentQuest == numberOfQuests)
        {
            // Set allQuestsCompleted to true
            allQuestsCompleted = true;
        }
        // Play the completion animation
        PlayCompletionAnimation();
        // Set current quest completed to false
        currentQuestCompleted = false;
        // Set questActive tyo false
        questActive = false;
    }
    /// <summary>
    /// Method that plays the animation when the player completes a quest
    /// </summary>
    private void PlayCompletionAnimation()
    {
        // If there are objects to animate
        if (questCompletionAnimatedObjects.Length >= currentQuest &&
            questCompletionAnimatedObjects.Length > 0)
        {
            // If the current quest object isn't null
            if (questCompletionAnimatedObjects[currentQuest - 1] != null)
            {
                //Make sure the item is active
                questCompletionAnimatedObjects[currentQuest - 1]
                    .gameObject.SetActive(true);
                // Get the item's animator component
                Animator animator = questCompletionAnimatedObjects[currentQuest - 1]
                    .GetComponent<Animator>();
                // If there is an animator, set it's Solved trigger
                animator?.SetTrigger("Solved");

                // If all quests have been completed
                if (allQuestsCompleted)
                {
                    // Go through all the objects in 
                    // questCompletionAnimatedObjects
                    for (int i = currentQuest - 1; i < questCompletionAnimatedObjects.Length; i++)
                    {
                        // If the item has "Monkey" in it's name
                        if (questCompletionAnimatedObjects[i].name.Contains("Monkey"))
                        {
                            // Set the monkey animator Solved trigger
                            Animator monkeyAnimator = questCompletionAnimatedObjects[i]
                                .GetComponent<Animator>();
                            monkeyAnimator?.SetTrigger("Solved");
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Method that checks the distance between the player and the NPC
    /// </summary>
    private void CheckDistanceFromPlayer()
    {
        //Mathematically calculate the distance between the player and this npc
        distance = Mathf.Sqrt(
            Mathf.Pow(transform.position.x - player.position.x, 2)
            + Mathf.Pow(transform.position.y - player.position.y, 2)
            + Mathf.Pow(transform.position.z - player.position.z, 2));
    }
    /// <summary>
    /// Method that turns this NPC to the player
    /// </summary>
    private void TurnToPlayer()
    {
        // Get npc rotation angle
        Quaternion npcRotationAngle = Quaternion.LookRotation(player.position - transform.position);
        // Get npc's panel rotation angle 
        Quaternion panelRotationAngle = Quaternion.LookRotation(questPanel.transform.position -
            new Vector3(player.position.x, player.position.y + 1, player.position.z));
        // Change the NPC's angle to face the player
        transform.rotation = Quaternion.Slerp(transform.rotation, npcRotationAngle, Time.deltaTime * 4);
        // Change the panel's angle to face the player
        questPanel.transform.rotation = Quaternion.Slerp(panelRotationAngle, questPanel.transform.rotation, Time.deltaTime * 4);
    }
    /// <summary>
    /// Method that will increment the quest timer
    /// </summary>
    private IEnumerator IncrementTimer()
    {
        while (true)
        {
            questTimer += Time.deltaTime;
            yield return null;
        }
    }
    /// <summary>
    /// Method that will reset the quest timer back to 0
    /// </summary>
    private void ResetTime()
    {
        questTimer = 0;
    }
    /// <summary>
    /// Method that will save the amount of time the player took to solve the current 
    /// quest into an array
    /// </summary>
    private void SaveTime()
    {
        SolvingTime[currentQuest] = Mathf.RoundToInt(questTimer);
    }
    /// <summary>
    /// Method that shows the exclamation point
    /// </summary>
    private void ShowExclamationPoint()
    {
        // Get the animator
        Animator animator = transform.Find("ExclamationPointHolder")
            .GetComponentInChildren<Animator>();
        // Set the Show trigger
        animator.SetTrigger("Show");
        //Increment the exclamationFlag
        exclamationFlag++;
    }
    /// <summary>
    /// Method that hides the exclamation point
    /// </summary>
    private void HideExclamationPoint()
    {
        // Get the animator
        Animator animator = transform.Find("ExclamationPointHolder")
            .GetComponentInChildren<Animator>();
        // Set the Hide trigger
        animator?.SetTrigger("Hide");
        //Set the exclamationFlag to zero
        exclamationFlag = 0;
    }
}
