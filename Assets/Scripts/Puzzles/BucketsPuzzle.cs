﻿using UnityEngine;

/// <summary>
/// A class that manages a Zebra's puzzle
/// </summary>
public class BucketsPuzzle : MonoBehaviour
{
    /// <summary>
    /// The quest item
    /// </summary>
    [SerializeField] private Item questItem;
    /// <summary>
    /// A red paint bucked item
    /// </summary>
    [SerializeField] private Item redPaintBucket;
    /// <summary>
    /// A green paint bucked item
    /// </summary>
    [SerializeField] private Item greenPaintBucket;
    /// <summary>
    /// A blue paint bucked item
    /// </summary>
    [SerializeField] private Item bluePaintBucket;
    /// <summary>
    /// A fountain item
    /// </summary>
    [SerializeField] private Item fountain;

    /// <summary>
    /// The player
    /// </summary>
    private Player player;
    /// <summary>
    /// A boolean set to true when the player has the red bucket
    /// </summary>
    private bool hasRed;
    /// <summary>
    /// A boolean set to true when the player has the green bucket
    /// </summary>
    private bool hasGreen;
    /// <summary>
    /// A boolean set to true when the player has the blue bucket
    /// </summary>
    private bool hasBlue;
    /// <summary>
    /// A flag set to true so that we only remove the red bucket from the
    /// inventory once
    /// </summary>
    private int redFlag;
    /// <summary>
    /// A flag set to true so that we only remove the green bucket from the
    /// inventory once
    /// </summary>
    private int greenFlag;
    /// <summary>
    /// A flag set to true so that we only remove the blue bucket from the
    /// inventory once
    /// </summary>
    private int blueFlag;
    /// <summary>
    /// A flag set to true so that we only unlock the fountain once
    /// </summary>
    private int fountainFlag;
    /// <summary>
    /// A flag set to true so that we only unlock the quest item once
    /// </summary>
    private int questItemFlag;
    /// <summary>
    /// Unity's standart Start method
    /// </summary>
    void Start()
    {
        // Get the player
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    /// <summary>
    /// Unity's standart Update method
    /// </summary>
    void Update()
    {
        // Check which buckets were picked while player hasn't picked all of 
        // them
        if (!(hasRed && hasGreen && hasBlue)) CheckPickedBuckets();
        // If the player has a red, a green or a blue bucket
        if (hasRed || hasGreen || hasBlue)
        {
            // If the fountain flag is zero, unlock it
            if (fountainFlag == 0) UnlockFountain();

            // If the player interacted with the fountain
            if (fountain.WasInteracted)
            {
                // If any of the flags is zero
                if (redFlag == 0 || greenFlag == 0 || blueFlag == 0)
                    RemoveItemsFromInventory();
            }
        }
        // If the fountain is disabled and the quest item flag is zero
        if (!fountain.enabled && questItemFlag == 0)
        {
            // Get the animator component
            Animator animator = GetComponent<Animator>();
            // Set the animator's boolean to true
            animator.SetBool("UnlockedQuestItem", true);
            // Increment the quest item flag
            questItemFlag++;
        }
    }
    /// <summary>
    /// Check which buckets have been picked
    /// </summary>
    private void CheckPickedBuckets()
    {
        if (player.HasInInventory(redPaintBucket) && !hasRed)
        {
            hasRed = true;
            Animator animator = GetComponent<Animator>();
            animator.SetBool("Red", true);
        }

        if (player.HasInInventory(greenPaintBucket) && !hasGreen)
        {
            hasGreen = true;
            Animator animator = GetComponent<Animator>();
            animator.SetBool("Green", true);
        }

        if (player.HasInInventory(bluePaintBucket) && !hasBlue)
        {
            hasBlue = true;
            Animator animator = GetComponent<Animator>();
            animator.SetBool("Blue", true);
        }
    }

    /// <summary>
    /// Unlock the fountain object
    /// </summary>
    private void UnlockFountain()
    {
        fountain.Unlock(true);
        fountainFlag++;
    }

    /// <summary>
    /// Remove from the player's inventory the items that he drops on the
    /// fountain
    /// </summary>
    private void RemoveItemsFromInventory()
    {

        if (hasRed && redFlag == 0)
        {
            player.RemoveFromInventory(redPaintBucket);
            redFlag++;
        }
        if (hasGreen && greenFlag == 0)
        {
            player.RemoveFromInventory(greenPaintBucket);
            greenFlag++;
        }
        if (hasBlue && blueFlag == 0)
        {
            player.RemoveFromInventory(bluePaintBucket);
            blueFlag++;
        }

    }
}
