﻿using UnityEngine;
/// <summary>
/// A class that manages the puzzle from the Tank Panel
/// </summary>
public class TankPanelPuzzle : MonoBehaviour
{
    /// <summary>
    /// Whether the puzzle has been solved or not
    /// </summary>
    public static bool Solved => winFlag;
    /// <summary>
    /// The levers that belong to the control panel gameobject
    /// </summary>
    [SerializeField] private Item[] levers = new Item[5];
    /// <summary>
    /// The lights that belong to the control panel gameobject
    /// </summary>
    [SerializeField] private PanelLights[] lights = new PanelLights[5];
    /// <summary>
    /// The tank's gate
    /// </summary>
    [SerializeField] private Transform tankGate;
    /// <summary>
    /// The dolphin NPC
    /// </summary>
    [SerializeField] private NPC dolphin;
    /// <summary>
    /// The sea lion NPC
    /// </summary>
    [SerializeField] private NPC seaLion;
    /// <summary>
    /// The sea lion's Animator
    /// </summary>
    [SerializeField] private Animator seaLionAnimator;
    /// <summary>
    /// The sea lion's Animator
    /// </summary>
    [SerializeField] private Transform seaLionNewPos;

    /// <summary>
    /// An array of booleans that are set to true if their corresponding lever 
    /// is active 
    /// </summary>
    private bool[] activeLever = new bool[4];

    /// <summary>
    /// A number that will be used to check whether the lever's lights should 
    /// be green or red
    /// </summary>
    private int lever1Number = 0;
    /// <summary>
    /// A number that will be used to check whether the lever's lights should 
    /// be green or red
    /// </summary>
    private int lever2Number = 0;
    /// <summary>
    /// A number that will be used to check whether the lever's lights should 
    /// be green or red
    /// </summary>
    private int lever3Number = 0;
    /// <summary>
    /// A number that will be used to check whether the lever's lights should 
    /// be green or red
    /// </summary>
    private int lever4Number = 0;

    /// <summary>
    /// A flag used to control the state of the lever 1
    /// </summary>
    private int counter1 = 0;
    /// <summary>
    /// A flag used to control the state of the lever 1
    /// </summary>
    private int counter2 = 0;
    /// <summary>
    /// A flag used to control the state of the lever 1
    /// </summary>
    private int counter3 = 0;
    /// <summary>
    /// A flag used to control the state of the lever 4
    /// </summary>
    private int counter4 = 0;

    /// <summary>
    /// 
    /// </summary>
    private bool leversFlag = false;
    /// <summary>
    /// A flag used to play the anims only when needed
    /// </summary>
    private bool animFlag = false;
    /// <summary>
    /// A flag used to play the anims only when needed
    /// </summary>
    private bool gateFlag = false;

    /// <summary>
    /// A flag used to only call ActivateMainLeverOnWin() when needed
    /// </summary>
    private static bool winFlag = false;
    /// <summary>
    /// A timer used to manage when some actions should take place
    /// </summary>
    private float timer = 0;

    /// <summary>
    /// Unity's standart Start method
    /// </summary>
    private void Start()
    {
        seaLionAnimator.enabled = false;
    }
    /// <summary>
    /// Unity's standart Update method
    /// </summary>
    private void Update()
    {
        CheckActiveLevers();
        ChangeLeverStates();
        ChangeLights();
        CheckLightStates();
        DisableSeaLionAnimation();
    }

    /// <summary>
    /// Verifies which levers are active and which are deactive
    /// </summary>
    private void CheckActiveLevers()
    {
        // Go through all the levers
        for (int i = 0; i < levers.Length - 1; i++)
        {
            //Set activeLever of the current lever to true if it was interacted
            if (levers[i].WasInteracted)
            {
                activeLever[i] = true;
            }
            //Else set activeLever of the current lever to false
            else
            {
                activeLever[i] = false;
            }
        }
    }

    /// <summary>
    /// Changes the states of the levers by incrementing or decrementing an 
    /// integer they are associated with. Those integers will be used to verify
    /// if the puzzle is solved or not
    /// </summary>
    private void ChangeLeverStates()
    {

        // If lever 1 is on
        if (activeLever[0] && counter1 == 0)
        {
            lever1Number++;
            lever2Number++;

            counter1++;
        }
        // If lever 1 is off
        if (!activeLever[0] && counter1 == 1)
        {
            lever1Number--;
            lever2Number--;

            counter1--;
        }

        // If lever 2 is on
        if (activeLever[1] && counter2 == 0)
        {
            lever2Number++;
            lever4Number++;

            counter2++;
        }
        // If lever 2 is off
        if (!activeLever[1] && counter2 == 1)
        {
            lever2Number--;
            lever4Number--;

            counter2--;
        }

        // If lever 3 is on
        if (activeLever[2] && counter3 == 0)
        {
            lever2Number++;
            lever3Number++;


            counter3++;
        }
        // If lever 3 is off
        if (!activeLever[2] && counter3 == 1)
        {
            lever2Number--;
            lever3Number--;

            counter3--;
        }

        // If lever 4 is on
        if (activeLever[3] && counter4 == 0)
        {
            lever2Number++;
            lever4Number++;

            counter4++;
        }
        // If lever 4 is off
        if (!activeLever[3] && counter4 == 1)
        {
            lever2Number--;
            lever4Number--;

            counter4--;
        }

    }

    /// <summary>
    /// Method that changes the lights of the panel
    /// </summary>
    private void ChangeLights()
    {
        //If number isn't even, change color to green
        if (lever1Number % 2 == 1)
        {
            lights[0].ChangeState(true);
        }
        //If number is even, change color to red
        else
        {
            lights[0].ChangeState(false);
        }

        //If number isn't even, change color to green
        if (lever2Number % 2 == 1)
        {
            lights[1].ChangeState(true);
        }
        //If number is even, change color to red
        else
        {
            lights[1].ChangeState(false);
        }

        //If number isn't even, change color to green
        if (lever3Number % 2 == 1)
        {
            lights[2].ChangeState(true);
        }
        //If number is even, change color to red
        else
        {
            lights[2].ChangeState(false);
        }

        //If number isn't even, change color to green
        if (lever4Number % 2 == 1)
        {
            lights[3].ChangeState(true);
        }
        //If number is even, change color to red
        else
        {
            lights[3].ChangeState(false);
        }

        // If the main lever is unlocked and active change it's panel color to 
        // green
        if (!levers[4].IsLocked)
        {
            lights[4].ChangeState(true);
        }
        else lights[4].ChangeState(false);

        bool flag = false;
        // If the main lever is locked
        if (leversFlag == true
            && (levers[4].IsLocked || !levers[4].WasInteracted))
        {
            if (gateFlag)
            {
                // Play the gate's close animation
                tankGate.GetComponent<Animator>().SetTrigger("Close");
                gateFlag = false;
            }
            leversFlag = false;
            flag = true;

        }
        //If the main lever was activated
        if (!leversFlag && !flag
            && !levers[4].IsLocked && levers[4].WasInteracted)
        {
            if (!gateFlag)
            {
                // Play the gate's open animation
                tankGate.GetComponent<Animator>().SetTrigger("Open");
                gateFlag = true;
            }

            if (!animFlag) timer += Time.deltaTime;

            // If animation flag is false and 1 second has passed
            if (!animFlag && timer > 1)
            {
                // Increment the quests of the dolphin and the sea lion
                dolphin.ChangeQuest();
                seaLion.ChangeQuest();


                seaLionAnimator.enabled = true;
                // Play sea lion's animation
                seaLionAnimator.SetTrigger("Solved");

                //Change sea lion's panel text
                seaLion.transform.Find("RiddlePanelHolder").GetChild(0).gameObject
                    .GetComponentInChildren<TextMesh>().text
                        = "Thank you so much for reuniting us!";
                //Change dolphin's panel text
                dolphin.transform.Find("RiddlePanelHolder").GetChild(0).gameObject
                    .GetComponentInChildren<TextMesh>().text
                        = "We really appreciate your help!";

                seaLion.GetComponent<DialogueAnimation>()
                    .SetDefaultFaces(0, 2, true);
                // Disable the NPC script of sea lion
                seaLion.enabled = false;


                // Turn flag to true so this code doesn't run again
                animFlag = true;
            }
            // If one second has passed, set leversFlag to true
            if (timer > 1) leversFlag = true;
        }
    }

    /// <summary>
    /// Method that unlocks the main lever when player hits the right 
    /// combination
    /// </summary>
    private void ActivateMainLeverOnWin()
    {
        //Unlock the main lever
        levers[4].Unlock(true);
        winFlag = true;
    }
    /// <summary>
    /// Method that verifies the state of each light
    /// </summary>
    private void CheckLightStates()
    {
        // Stop the method if any of the lights isn't on
        for (int i = 0; i < lights.Length - 1; i++)
        {
            // If the current light is inactive
            if (!lights[i].Active)
            {
                // If the lever is on, turn it off
                if (levers[4].WasInteracted) levers[4].Interact();
                // Lock the lever
                if (winFlag) levers[4].Unlock(false);

                winFlag = false;
                return;
            }
        }
        // If win flag is false activate the main lever
        if (!winFlag) ActivateMainLeverOnWin();
    }
    /// <summary>
    /// Method that disables the sea lion's animator
    /// </summary>
    private void DisableSeaLionAnimation()
    {
        // If the sea lion is at his new position
        if (seaLion.transform.position == seaLionNewPos.position)
        {
            // Set the sea lion's animator to false
            seaLionAnimator.enabled = false;
            // Set the sea lion NPC script to true
            seaLion.enabled = true;
        }
    }

}


