﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// The class that manages the player's inventory
/// </summary>
public class Inventory : MonoBehaviour
{
    #region Singleton
    /// <summary>
    /// An instance of this class, used to implement a singleton
    /// </summary>
    private static Inventory instance;
    /// <summary>
    /// A property of this class type that returns the instance of itself
    /// (Singleton)
    /// </summary>
    public static Inventory Instance
    {
        get { return instance; }
    }
    /// <summary>
    /// Unity's standard Awake method
    /// </summary>
    void Awake()
    {
        // If instance is null, set it to this class
        if (instance == null)
            instance = this;
    }
    #endregion
    /// <summary>
    /// The sprite for the inventory
    /// </summary>
    [SerializeField] internal GameObject inventorySprite;
    /// <summary>
    /// A Prefab GameObject used to instantiate a slot on the inventory for
    /// each picked object
    /// </summary>
    [SerializeField] internal GameObject prefabSlot;
    /// <summary>
    /// An array that contains the sprites for the scribbled text
    /// </summary>
    [SerializeField] internal Sprite[] scribbles;
    /// <summary>
    /// A list of Items (the actual inventory)
    /// </summary>
    internal List<Item> itemList = new List<Item>();
    /// <summary>
    /// A text that will be contained in the slot and will be set to the name 
    /// of the picked object
    /// </summary>
    private Text text;
    /// <summary>
    /// An image that will be contained in the slot and will be used to 
    /// scribble inventory items that have been removed from it (like a
    /// grocery list)
    /// </summary>
    private Image image;
    /// <summary>
    /// A GameObject that will be set to instances of the prefabSlot prefab
    /// </summary>
    private GameObject slot;
    /// <summary>
    /// The holder of the slots
    /// </summary>
    private GameObject slotsHolder;
    /// <summary>
    /// Method that adds the picked item to the player's inventory
    /// </summary>
    /// <param name="item">The item to add to the inventory.</param>
    public void Add(Item item)
    {
        // If the item is not a monkey
        if (!item.name.Contains("Monkey"))
        {
            // Add item to the itemList
            itemList.Add(item);

            // Instanciate a new slot in the slots holder
            slot = Instantiate(prefabSlot,
                inventorySprite.transform.GetChild(0).transform.GetChild(0));
            // Change the text name of the slot to the name of the item
            ChangeTextName(item);
        }
        // If the item is a monkey
        else
        {
            // Get the game manager
            GameManager gameManager =
                GameObject.Find("GameManager").GetComponent<GameManager>();
            // Call the gameManager's method for when we catch a monkey
            gameManager.CaughtMonkey(item);
            // Find and play the sound of the object "MonkeyScream"
            GameObject.FindGameObjectWithTag("MonkeyScream")
                .GetComponent<AudioSource>().Play(); ;
        }
        // Deactivate the picked GameObject
        item.gameObject.SetActive(false);
    }
    /// <summary>
    /// Method that removes the item from the inventory
    /// </summary>
    /// <param name="item">The item to remove from the inventory</param>
    public void Remove(Item item)
    {
        // Remove the item from the itemList
        itemList.Remove(item);
        // Scribble the name of that item off of the list
        ScribbleNameOfList(item);
    }
    /// <summary>
    /// Method that returns true if the inventory contains the item in the
    /// parameter
    /// </summary>
    /// <param name="item">The item we want to check if it's on the inventory.
    /// </param>
    /// <returns></returns>
    public bool Contains(Item item)
    {
        return itemList.Contains(item);
    }
    /// <summary>
    /// Method that changes the slot's text to the name of the item sent as a
    /// parameter
    /// </summary>
    /// <param name="item">The item whose name we want to write on the slot.
    /// </param>
    private void ChangeTextName(Item item)
    {
        // Get the Text component
        text = slot.gameObject.GetComponentInChildren<Text>();
        // Change the text to the item name
        text.text = item.ItemName;

        // Make sure that the scribble sprite is off
        text.transform.parent.GetChild(0).GetComponent<Image>().color = new Color(0, 0, 0, 0);
    }
    /// <summary>
    /// Method that show an image of a scribble on top of the slot of the item
    /// we removed  from the inventory
    /// </summary>
    /// <param name="item">The item we removed from the inventory.</param>
    private void ScribbleNameOfList(Item item)
    {
        // Get a random number between 1 and 2
        int rand = Random.Range(1, 3);

        // Get the slots holder
        slotsHolder = inventorySprite.transform.GetChild(0)
            .transform.GetChild(0).gameObject;
        // Get all of the Text's in the slots holder into an array of Text
        Text[] itemTexts = slotsHolder.GetComponentsInChildren<Text>();

        // Go through all of the Text's in itemTexts
        for (int i = 0; i < itemTexts.Length; i++)
        {
            // If the current text is the same as the item sent has a parameter
            if (itemTexts[i].text == item.ItemName) text = itemTexts[i];
        }
        // If the text isn't null
        if (text != null)
        {
            // Get the image related to that text
            image = text.transform.parent.GetChild(0).GetComponent<Image>();


            // Just a precaution in case the text wasn't null but it's length
            // was 0
            if (text.text.Length > 0)
            {
                image.color = new Color(1, 1, 1, 1);
            }

            // If the text has 1 to 5 characters change it's sprite to one of 
            // the three small scribble sprites (randomly)
            if (text.text.Length >= 1 && text.text.Length < 6)
            {
                // Set the position of the image
                image.rectTransform.anchoredPosition =
                    new Vector2(-85, image.rectTransform.anchoredPosition.y);
                // Randomly set the sprite
                switch (rand)
                {
                    case 1:
                        image.sprite = scribbles[0];
                        break;
                    case 2:
                        image.sprite = scribbles[1];
                        break;
                    case 3:
                        image.sprite = scribbles[2];
                        break;
                }
            }
            // If the text has 6 to 9 characters change it's sprite to one of 
            // the three medium scribble sprites (randomly)
            if (text.text.Length >= 6 && text.text.Length < 10)
            {
                // Set the position of the image
                image.rectTransform.anchoredPosition =
                    new Vector2(-73, image.rectTransform.anchoredPosition.y);
                // Randomly set the sprite
                switch (rand)
                {
                    case 1:
                        image.sprite = scribbles[3];
                        break;
                    case 2:
                        image.sprite = scribbles[4];
                        break;
                    case 3:
                        image.sprite = scribbles[5];
                        break;
                }
            }
            // If the text has more than 10 characters change it's sprite to 
            // one of the three big scribble sprites (randomly)
            if (text.text.Length >= 10)
            {
                // Set the position of the image
                image.rectTransform.anchoredPosition =
                    new Vector2(-50, image.rectTransform.anchoredPosition.y);
                // Randomly set the sprite
                switch (rand)
                {
                    case 1:
                        image.sprite = scribbles[6];
                        break;
                    case 2:
                        image.sprite = scribbles[7];
                        break;
                    case 3:
                        image.sprite = scribbles[8];
                        break;
                }
            }
        }
    }
    /// <summary>
    /// Method that shows the inventory
    /// </summary>
    public void ShowInventory()
    {
        // Set the inventory's animator boolean to true
        inventorySprite.GetComponent<Animator>().SetBool("Visible", true);
    }
    /// <summary>
    /// Method that hides the inventory
    /// </summary>
    public void HideInventory()
    {
        // Set the inventory's animator boolean to false
        inventorySprite.GetComponent<Animator>().SetBool("Visible", false);
    }
}
