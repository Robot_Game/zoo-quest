﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// The class that manages the main menu
/// </summary>
public class MainMenu : MonoBehaviour {

	/// <summary>
    /// Unity's standard Start method
    /// </summary>
	void Start ()
    {
        // Make the cursor visible
        Cursor.visible = true;
        // Unlock the cursor
        Cursor.lockState = CursorLockMode.None;
        // Destroy objects we don't want to have on the main menu
        DestroyUnwantedObjects();

	}
    /// <summary>
    /// Method called when the player presses the Play button
    /// </summary>
    public void Play()
    {
        // Load the Main Scene
        SceneManager.LoadScene("MainScene");
    }
    /// <summary>
    /// Method called when the player presses the Quit button
    /// </summary>
    public void Quit()
    {
        // Quit the game
        Application.Quit();
    }
    /// <summary>
    /// Method called when the player presses the Credits button
    /// </summary>
    public void ShowCredits()
    {
        // Set ShowCredits trigger from the main menu animator
        GetComponent<Animator>().SetTrigger("ShowCredits");
    }
    /// <summary>
    /// Method called when the player presses the Back button on the credits
    /// menu
    /// </summary>
    public void HideCredits()
    {
        // Set HideCredits trigger from the main menu animator
        GetComponent<Animator>().SetTrigger("HideCredits");
    }
    /// <summary>
    /// Method that destroys objects we don't want to have on the main menu
    /// </summary>
    private void DestroyUnwantedObjects()
    {
        // Get the player GameObject
        GameObject player = GameObject.Find("Player");
        // Get the canvas GameObject
        GameObject canvas = GameObject.Find("Canvas");
        //If there is a player GameObject, destroy it
        if (player != null) Destroy(player);
        //If there is a canvas GameObject, destroy it
        if (canvas != null) Destroy(canvas);
    }
}
