﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
/// <summary>
/// This class is used to change some of the game's characteristics, such as
/// players movements speeed.
/// </summary>
public class Cheats : MonoBehaviour
{
    /// <summary>
    /// An array that holds objects with different positions within the world
    /// which will be used to teleport the player
    /// </summary>
    [SerializeField] private Transform[] teleportPoints;
    /// <summary>
    /// A float used to change the players speed
    /// </summary>
    [SerializeField] private float movementSpeedMultiplier;
    /// <summary>
    /// The Unity's standard FirstPersonController asset script
    /// </summary>
    private FirstPersonController player;
    /// <summary>
    /// A boolean that holds whether the cheats are on or off
    /// </summary>
    private bool cheatsOn = false;

    // Unity's standard Start method in which we get the player's 
    // FirstPersonController component
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player")
            .GetComponent<FirstPersonController>();
    }

    // Unity's standard Update method
    void Update()
    {
        // Call a method that checks for input and turns the cheats on based
        // on that input
        ManageCheatsActivation();
    }
    /// <summary>
    /// Activate cheats or teleport the player depending on the pressed keys
    /// </summary>
    private void ManageCheatsActivation()
    {
        // If player presses F
        if (Input.GetKeyUp(KeyCode.F))
        {
            // Deactivate cheats if they are on
            if (cheatsOn) TurnCheatsOff();
            // Activate the cheats if they are off
            else TurnCheatsOn();
        }
        // If player presses the 1 key teleport him to location 1
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            player.transform.position = teleportPoints[0].position;
        }
        // If player presses the 2 key teleport him to location 2
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            player.transform.position = teleportPoints[1].position;
        }
        // If player presses the 3 key teleport him to location 3
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            player.transform.position = teleportPoints[2].position;
        }

    }
    /// <summary>
    /// Deactivate cheats
    /// </summary>
    private void TurnCheatsOff()
    {
        // Divide player's speed by the multiplier
        player.m_WalkSpeed = player.m_WalkSpeed / movementSpeedMultiplier;
        // Divide player's run speed by the multiplier
        player.m_RunSpeed = player.m_RunSpeed / movementSpeedMultiplier;
        // Turn head bob on
        player.m_UseHeadBob = true;
        cheatsOn = false;
    }
    /// <summary>
    /// Activate cheats
    /// </summary>
    private void TurnCheatsOn()
    {
        // Multiply player's speed by the multiplier
        player.m_WalkSpeed = player.m_WalkSpeed * movementSpeedMultiplier;
        // Multiply player's run speed by the multiplier
        player.m_RunSpeed = player.m_RunSpeed * movementSpeedMultiplier;
        // Turn head bob off
        player.m_UseHeadBob = false;
        cheatsOn = true;
    }
}
