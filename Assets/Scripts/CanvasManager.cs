﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class is used to manage the canvas
/// </summary>
public class CanvasManager : MonoBehaviour
{
    /// <summary>
    /// An instance of this class, used to implement a singleton
    /// </summary>
    private static CanvasManager instance;
    /// <summary>
    /// The Image for the pause menu
    /// </summary>
    [SerializeField] private Image pauseMenu;
    /// <summary>
    /// The UI panel that displays interaction texts
    /// </summary>
    [SerializeField] private GameObject interactionPanel;
    /// <summary>
    /// The text which describes the available interaction
    /// </summary>
    [SerializeField] private Text interactionText;
    /// <summary>
    /// The panel's sprite for when it's text is small
    /// </summary>
    [SerializeField] private Sprite smallSprite;
    /// <summary>
    /// The panel's sprite for when it's text is big
    /// </summary>
    [SerializeField] private Sprite bigSprite;
    /// <summary>
    /// The Animator component of the interaction panel
    /// </summary>
    private Animator panelAnimator;
    /// <summary>
    /// Flag used to manage when to show the panel
    /// </summary>
    private int showFlag;
    /// <summary>
    /// Flag used to manage when to hide the panel
    /// </summary>
    private int hideFlag;

    /// <summary>
    /// A property of this class type that returns the instance of itself
    /// (Singleton)
    /// </summary>
    public static CanvasManager Instance
    {
        get { return instance; }
    }

    /// <summary>
    /// Unity's standard Awake method in which we call DDOL so that the object
    /// this script is attached to doesn't get destroyed when changing scenes
    /// </summary>
    void Awake()
    {
        // Don't destroy the object this script is attached to when changing
        // scenes
        DontDestroyOnLoad(gameObject);
        // If instance is null, set it to this class
        if (instance == null)
            instance = this;

        // Lock the cursor to the game window
        Cursor.lockState = CursorLockMode.Confined;
    }
    /// <summary>
    /// Unity's standard Start method in which we get the interaction panel's
    /// animator component and we call a method that hides the panel
    /// </summary>
    void Start()
    {
        // Get the panel
        panelAnimator = transform.Find("InteractionPanel").GetComponent<Animator>();
        // Hide the panel
        HideInteractionPanel();

    }
    /// <summary>
    /// Method that hides the interaction panel
    /// </summary>
    public void HideInteractionPanel()
    {
        // Stop the coroutine that resets the hideFlag
        StopCoroutine(ResetHideFlag());

        // If hideFlag is zero
        if (hideFlag == 0)
        {
            // Set the hiding trigger of the panel's animator, causing it's 
            // hide animation to start
            if (panelAnimator != null)
                panelAnimator.SetTrigger("Hide");
            // Increment the hideFlag
            hideFlag++;
            // Start a coroutine that resets the hideFlag to zero after a brief
            // delay
            StartCoroutine(ResetShowFlag());
        }
    }

    /// <summary>
    /// Shows or hides the pause menu
    /// </summary>
    /// <param name="show">Boolean that determines whether to show or hide the
    /// pause menu</param>
    public void ShowPauseMenu(bool show)
    {
        // If we want to show the pause menu, enable it
        if (show) pauseMenu.enabled = true;
        // Else, disable it
        else pauseMenu.enabled = false;
    }
    
    /// <summary>
    /// Shows the interaction panel
    /// </summary>
    /// <param name="text">The text that will be displayed in the panel.
    /// </param>
    public void ShowInteractionPanel(string text)
    {
        // Stop the coroutine that resets the showFlag
        StopCoroutine(ResetShowFlag());

        // Change the interaction text to the given one
        interactionText.text = text;
        // If the text has more than 32 characters, set the panel sprite to the
        // bigSprite
        if (interactionText.text.Length > 32)
        {
            interactionPanel.GetComponent<Image>().sprite = bigSprite;
        }
        // Else set the panel sprite to the smallSprite
        else
        {
            interactionPanel.GetComponent<Image>().sprite = smallSprite;
        }
        // If the showFlag is zero
        if (showFlag == 0)
        {
            // Set the trigger of the show animation
            if (panelAnimator != null)
                panelAnimator.SetTrigger("Show");
            // Increment the showFlag
            showFlag++;
            // Start the coroutine that resets the hideFlag
            StartCoroutine(ResetHideFlag());
        }
    }
    /// <summary>
    /// Show the animation of an UI element that contains the number of monkeys
    /// caught
    /// </summary>
    /// <param name="caughtMonkeys">The number of monkeys caught.</param>
    public void ShowMonkeyCount(int caughtMonkeys)
    {
        // Get the animator component of the object that shows the number of
        // caught monkeys
        Animator animator = transform.Find("MonkeyCounter")
            .GetComponent<Animator>();
        // Get the text of that same object (which is a component on one of 
        // it's childs
        Text text = animator.GetComponentInChildren<Text>();
        // Change the text to display the current number of caught monkeys
        text.text = "Monkeys Caught: " + caughtMonkeys;
        // Set the animators trigger to start the animation
        animator.SetTrigger("Show");
    }
    /// <summary>
    /// Wait a very small interval before reseting the showFlag. We implemented
    /// this for a smooth animation in the interaction panel.
    /// </summary>
    /// <returns></returns>
    IEnumerator ResetShowFlag()
    {
        // Wait 0.05 seconds before continuing
        yield return new WaitForSeconds(0.05f);
        // Set the showFlag to 0
        showFlag = 0;
    }

    /// <summary>
    /// Wait a very small interval before reseting the hideFlag. We implemented
    /// this for a smooth animation in the interaction panel.
    /// </summary>
    /// <returns></returns>
    IEnumerator ResetHideFlag()
    {
        // Wait 0.05 seconds before continuing
        yield return new WaitForSeconds(0.05f);
        // Set the hideFlag to 0
        hideFlag = 0;
    }
}
