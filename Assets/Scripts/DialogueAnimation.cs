﻿using UnityEngine;

/// <summary>
/// Class that manages the dialogue animations of the NPCs
/// </summary>
public class DialogueAnimation : MonoBehaviour
{
    /// <summary>
    /// An array of sprites that holds all the different sprites for the eyes
    /// </summary>
    [SerializeField] private Sprite[] eyesSprites;
    /// <summary>
    /// An array of sprites that holds all the different sprites for the mouth
    /// </summary>
    [SerializeField] private Sprite[] mouthSprites;
    /// <summary>
    /// The sprite renderer of the left eye
    /// </summary>
    private SpriteRenderer leftEye;
    /// <summary>
    /// The sprite renderer of the right eye
    /// </summary>
    private SpriteRenderer rightEye;
    /// <summary>
    /// The sprite renderer of the mouth
    /// </summary>
    private SpriteRenderer mouth;
    /// <summary>
    /// An int that will hold a random value, on which the mouth's sprite will
    /// depend
    /// </summary>
    private int mouthNumber;
    /// <summary>
    /// An int that will hold a random value, on which the eyes' sprite will
    /// depend
    /// </summary>
    private int eyesNumber;
    /// <summary>
    /// An integer used to make a custom loop
    /// </summary>
    private int i = 1;
    /// <summary>
    /// A flag used to manage which portion of the code should be running
    /// </summary>
    private int flag = 0;
    /// <summary>
    /// The interval used between sprite swaps
    /// </summary>
    private float interval = 0.15f;

    /// <summary>
    /// Unity's standard Start method in which we get the components needed
    /// </summary>
    void Start()
    {
        // Get the left eye SpriteRenderer
        leftEye = transform.Find("FaceSprites").Find("LeftEye")
            .GetComponent<SpriteRenderer>();
        // Get the right eye SpriteRenderer
        rightEye = transform.Find("FaceSprites").Find("RightEye")
            .GetComponent<SpriteRenderer>();
        // Get the mouth SpriteRenderer
        mouth = transform.Find("FaceSprites").Find("Mouth")
            .GetComponent<SpriteRenderer>();
    }
    /// <summary>
    /// Set the sprites of the eyes and mouth to the default ones
    /// </summary>
    /// <param name="eyessprite">The index of the array that holds the sprite
    /// we want as default.</param>
    /// <param name="mouthsprite">The index of the array that holds the sprite
    /// we want as default.</param>
    /// <param name="showMouth">Whether or not we want the mouth to stay 
    /// visible.</param>
    public void SetDefaultFaces(int eyessprite, int mouthsprite, bool showMouth)
    {
        // Change the sprites
        leftEye.sprite = eyesSprites[eyessprite];
        rightEye.sprite = eyesSprites[eyessprite];
        mouth.sprite = mouthSprites[mouthsprite];

        // If we don't want the mouth to be seen, change it's alpha 
        // (transparency) to zero
        if (!showMouth) mouth.color = new Color(0, 0, 0, 0);
        // Set i to it's initial value
        i = 1;
        // Set the flag to it's initial value
        flag = 0;
    }

    /// <summary>
    /// This method contains a sort of loop in which, after a certain interval,
    /// we randomly change the sprites of the eyes and mouths. The mouth will 
    /// change every interval, while the eyes will only change every three 
    /// intervals.
    /// </summary>
    /// <param name="timer">An outside timer that is used to manage when the
    /// sprites should be changed.</param>
    public void ShowAnimations(float timer)
    {
        // Change the mouth renderer transparency to 100 so we can see it again
        // (in case it was at 0)
        mouth.color = new Color(1, 1, 1, 1);

        // If the timer is bigger than (interval * i) and the flag is zero
        // (if this is the first lap of the loop, i will be 1. If it is the
        // second lap, i will be 4. And so on...
        if (timer > interval * i && flag == 0)
        {
            // Increment the flag and i
            flag++;
            i++;

            // Get random values for the mouthNumber and eyeNumber
            GetRandomForMouth(mouthNumber);
            GetRandomForEyes(eyesNumber);

            // Set the mouth's sprite using the random number previously 
            // generated
            mouth.sprite = mouthSprites[mouthNumber];
            // Set the eyes' sprites using the random number previously 
            // generated
            leftEye.sprite = eyesSprites[eyesNumber];
            rightEye.sprite = eyesSprites[eyesNumber];


        }

        // Do the same has in the previous if statement, but now only change
        // the mouth's sprite
        if (timer > interval * i && flag == 1)
        {
            flag++;
            i++;

            GetRandomForMouth(mouthNumber);
            GetRandomForEyes(eyesNumber);

            mouth.sprite = mouthSprites[mouthNumber];
        }
        // Do the same has in the previous if statement,  only changing the 
        // mouth's sprite again
        if (timer > interval * i && flag == 2)
        {
            flag++;
            i++;

            GetRandomForMouth(mouthNumber);
            GetRandomForEyes(eyesNumber);

            mouth.sprite = mouthSprites[mouthNumber];
        }
        // Wait another interval and then reset the flag to zero so that we go 
        // back to the beggining of the loop. This prevents two sprite swaps 
        // from happening too fast
        if (timer > interval * i && flag == 3)
        {
            flag = 0;
        }
    }

    /// <summary>
    /// Sets the mouthNumber to a random value, between 0 and the amound of 
    /// sprites inside the mouthSprites array. If the random number is equal
    /// to the previous one, the method calls it self.
    /// </summary>
    /// <param name="previousNumber">The previous randomly generated 
    /// number.</param>
    private void GetRandomForMouth(int previousNumber)
    {
        // Get random number
        mouthNumber = Random.Range(0, mouthSprites.Length);
        // If the generated number is the same as the previous one, call this
        // method again
        if (mouthNumber == previousNumber) GetRandomForMouth(mouthNumber);
    }
    /// <summary>
    /// Sets the eyesNumber to a random value, between 0 and the amound of 
    /// sprites inside the eyesSprites array. If the random number is equal
    /// to the previous one, the method calls it self.
    /// </summary>
    /// <param name="previousNumber">The previous randomly generated 
    /// number.</param>
    private void GetRandomForEyes(int previousNumber)
    {
        // Get random number
        eyesNumber = Random.Range(0, eyesSprites.Length);
        // If the generated number is the same as the previous one, call this
        // method again
        if (eyesNumber == previousNumber) GetRandomForMouth(eyesNumber);
    }

}
