using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Class used to run the background music at all times
/// </summary>
public class DDOLMusic : MonoBehaviour
{
    /// <summary>
    /// Float that will be used to divide sound's volume
    /// </summary>
    [SerializeField] private float divisor;
    /// <summary>
    /// Flag used to make sure we only divide the volume once
    /// </summary>
    private bool flag = false;

    /// <summary>
    /// Unity's standard Awake method in which we find the object that plays
    /// the background music and runs the DDOL
    /// </summary>
    void Awake()
    {
        // Find an object of type DDOLMusic
        DDOLMusic bgMusic = FindObjectOfType<DDOLMusic>();
        // If that object's name is "BackgroundMusic" and it isn't this object
        if (bgMusic.name == "BackgroundMusic" && bgMusic != this)
            // Destroy this GameObject
            Destroy(gameObject);

        // Prevent this object from getting destroyed when changing scenes
        DontDestroyOnLoad(gameObject);
    }
    /// <summary>
    /// Unity's standard Update method
    /// </summary>
    private void Update()
    {
        // If we are playing in the main scene, turn volume down
        if (IsMainScene() && !flag) TurnVolumeDown();
        // Else, turn volume up
        if (!IsMainScene() && flag) TurnVolumeUp();
    }
    /// <summary>
    /// Method that returns true if we are in the main scene and false if we aren't
    /// </summary>
    /// <returns></returns>
    private bool IsMainScene()
    {
        // If the active scene's name is "MainScene" return true
        if (SceneManager.GetActiveScene().name == "MainScene")
        {
            return true;
        }
        // Else return false
        return false;
    }
    /// <summary>
    /// Method that turns the volume down
    /// </summary>
    private void TurnVolumeDown()
    {
        GetComponent<AudioSource>().volume /= divisor;
        flag = true;
    }
    /// <summary>
    /// Method that turns the volume up
    /// </summary>
    private void TurnVolumeUp()
    {
        GetComponent<AudioSource>().volume *= divisor;
        flag = false;
    }
}
