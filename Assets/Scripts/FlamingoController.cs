﻿using System.Collections;
using UnityEngine;

/// <summary>
/// A class that manages the Flamingos behaviours
/// </summary>
public class FlamingoController : MonoBehaviour {

    /// <summary>
    /// Array that contains the different sprites for the eyes
    /// </summary>
    [SerializeField] private Sprite[] eyesSprite;
    /// <summary>
    /// The left eye SpriteRenderer
    /// </summary>
    private SpriteRenderer leftEye;
    /// <summary>
    /// The right eye SpriteRenderer
    /// </summary>
    private SpriteRenderer rightEye;

	/// <summary>
    /// Unity's standard Start method
    /// </summary>
	void Start () {
        // Get the left eye's sprite renderer
        leftEye = transform.Find("Eyes").Find("LeftEye")
            .GetComponent<SpriteRenderer>();
        // Get the right eye's sprite renderer
        rightEye = transform.Find("Eyes").Find("RightEye")
            .GetComponent<SpriteRenderer>();
        // Start a coroutine that makes the flamingos blink randomly between
        // a certain interval
        StartCoroutine(Blink());
    }

    /// <summary>
    /// A coroutine used to make the flamingos blink
    /// </summary>
    /// <returns></returns>
    private IEnumerator Blink()
    {
        // Infinite loop
        while (true)
        {
            // Wait a random time between a certain interval
            yield return new WaitForSeconds(GetRandomTime());

            // Close flamingos' eyes
            CloseEyes();

            // Wait 0.3 seconds
            yield return new WaitForSeconds(0.3f);

            // Open flamingos' eyes
            OpenEyes();
        }
    }

    /// <summary>
    /// Method that changes the sprites of the flamingo's eyes from an opened
    /// eye sprite to a closed eye sprite
    /// </summary>
    private void CloseEyes()
    {
        leftEye.sprite = eyesSprite[1];
        rightEye.sprite = eyesSprite[1];
    }
    /// <summary>
    /// Method that changes the sprites of the flamingo's eyes from a closed
    /// eye sprite to an opened eye sprite
    /// </summary>
    private void OpenEyes()
    {
        leftEye.sprite = eyesSprite[0];
        rightEye.sprite = eyesSprite[0];
    }

    /// <summary>
    /// Method that returns a random float between 0.2 and 3
    /// </summary>
    /// <returns>Random float between 0.2 and 3.</returns>
    private float GetRandomTime()
    {
        return Random.Range(0.2f, 3f);
    }
}
