﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using UnityEngine;

public class DatabaseManager : MonoBehaviour
{
    public DatabaseConnectionParams remoteConnectionParams;

    [Header("Table Names")]
    public string tablePlanets = "GD_Planets";
    public string tableResources = "GD_Resources";

    SqlConnection connection;

    public bool Connect()
    {
        if (connection != null) return true;

        string connectionString = "";

        connectionString += "Data Source=" + remoteConnectionParams.dataSource + ";";
        connectionString += "Initial Catalog=" + remoteConnectionParams.databaseName + ";";
        connectionString += "User ID=" + remoteConnectionParams.username + ";";
        connectionString += "Password=" + remoteConnectionParams.password + ";";

        connection = new SqlConnection(connectionString);
        try
        {
            Debug.Log("Connecting...");
            connection.Open();
        }
        catch (Exception e)
        {
            Debug.LogError("Failed to connect: " + e.Message);
            return false;
        }

        Debug.Log("Connection established!");

        return true;
    }

    bool RunQuery(string query)
    {
        SqlCommand sql = new SqlCommand(query, connection);
        try
        {
            sql.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            Debug.LogError("Failed to run query: " + query);
            Debug.LogError("Error: " + e.Message);
            return false;
        }

        Debug.Log(query);

        return true;
    }
}
